package com.conduent.databroker.my_kafka.controller;

import com.conduent.databroker.my_kafka.KafkaSender;
import com.conduent.databroker.my_kafka.model.EventHitModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/kafka")
@RestController
public class KafkaController {
    @Autowired
    KafkaSender kafkaSender;

    @PostMapping("/{topicName}")
    public String enqueueEventHit(@PathVariable String topicName, @RequestBody EventHitModel model) {
        kafkaSender.send(topicName, model);
        return "Message sent";
    }
}
