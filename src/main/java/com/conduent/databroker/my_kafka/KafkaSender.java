package com.conduent.databroker.my_kafka;


import com.conduent.databroker.my_kafka.model.EventHitModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
    public class KafkaSender {

        @Autowired
        private KafkaTemplate<Object, EventHitModel> kafkaTemplate;

        public void send(String topic, EventHitModel model) {
            kafkaTemplate.send(topic, model);
            System.out.println("Message: "+model+" sent to topic: "+topic);
        }


    }

